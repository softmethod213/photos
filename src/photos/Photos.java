package photos;

/**
 * Photos main class.
 * @author Daniel Kim
 * @author George Ding
 */

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import controller.LoginController;
import controller.PhotoController;
import javafx.application.Application;
import javafx.stage.Stage;
import object.Album;
import javafx.fxml.*;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;


public class Photos extends Application{

	@FXML private static AnchorPane loginScreen;
	
	/**
	 * Used for Serializing at stage close from the X button. Added to at login
	 * and removed by logout button.
	 */
	public static ArrayList<ArrayList<Album>> toSerializeList = new ArrayList<ArrayList<Album>>();
	/**
	 * User to serialize at stage close from the X button. Added to at login
	 * and remove by logout button.
	 */
	public static ArrayList<String> userList = new ArrayList<String>();
	
	/**
	 * Main application start. Loads in FXML for Login Screen and username data.
	 * @param stage Stage
	 */
	public void start(Stage stage) {
		
		File dir = null;
		if(!(dir = new File("/src/Login Data")).exists()) {
			dir.mkdir();
		}
		
		File loginFile = new File("src/Login Data/user");
		
		if(!loginFile.exists()) {
			FileWriter fw;
			try {
				fw = new FileWriter(loginFile);
	
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write("stock\n");
				fw.close();
			}catch (IOException e) {
				//shouldn't happen
			}
		}
		
		File dir2 = new File("/src/Photo Data");
		if(!dir2.exists()) {
			dir2.mkdir();
		}
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/fxml/LoginScreen.fxml"));
		
		try {
			
			loginScreen = (AnchorPane) loader.load();
			
			LoginController lc = loader.getController();
			
			lc.start(stage, loginFile);
		
			Scene scene = new Scene(loginScreen);
			stage.setScene(scene);
			stage.setResizable(false);
			stage.show();
			
		}catch (IOException e) {
			System.out.println(e);
			//Login FXML missing
		}
	}
	
	/**
	 * Main. At close of launch(args), serializes any leftover data not serialized
	 * by logout button.
	 */
	public static void main(String[] args) {
		
		launch(args);
		
		PhotoController.backToAlbum();
		
		for(int i=0; i<toSerializeList.size(); i++) {
			try {
				FileOutputStream fos = new FileOutputStream("src/Photo Data/" + userList.get(i) + ".ser");
				BufferedOutputStream bos = new BufferedOutputStream(fos);
				ObjectOutputStream oos = new ObjectOutputStream(bos);
				
				oos.writeObject(toSerializeList.get(i));
				
				oos.close();
				bos.close();
				fos.close();
			} catch (FileNotFoundException e1) {
				
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

}
