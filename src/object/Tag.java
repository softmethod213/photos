package object;

import java.io.Serializable;

/**
 * Tag Object
 *
 */
public class Tag implements Serializable{
	
	private static final long serialVersionUID = -8777274900701783175L;
	public static final boolean exists = true;
	private String tagInfo;
	private String tagType;	
	
	
	/**
	 * Constructor for a Tag Object
	 * @param tagType The tag Type
	 * @param tagInfo The tag Description
	 */
	public Tag(String tagType, String tagInfo) {
		this.tagType = tagType;
		this.tagInfo = tagInfo;
	}
	
	/**
	 * Getting the instance's tag info
	 * @return this.tagInfo
	 */
	public String gettagInfo() {
		return this.tagInfo;
	}
	
	/**
	 * Getting the instance's tag type
	 * @return this.tagType
	 */
	public String gettagType() {
		return this.tagType;
	}	

}
