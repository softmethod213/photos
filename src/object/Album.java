package object;
/**
 * Album Object
 * @author Daniel Kim
 * @author George Ding
 */

import java.util.ArrayList;
import java.util.Date;

import javafx.beans.property.StringProperty;

import java.io.*;

public class Album implements Serializable{

	
	private static final long serialVersionUID = 1166018934775792613L;

	private ArrayList<Photo> photoList;
	
	private Date oldest;
	
	private Date newest;
	
	private String name;
	
	public int items;
	
	/**
	 * Constructor for new Album
	 * @param name String
	 */
	public Album(String name) {
		this.name = name;
		this.items = 0;
		this.photoList = new ArrayList<Photo>();
	}
	
	/**
	 * 
	 * Constructor for new Album with search results as the list
	 * @param name String
	 * @param photoList ArrayList<Photo>
	 */
	public Album(String name, ArrayList<Photo> photoList) {
		this.name = name;
		this.items = photoList.size();
		this.photoList = photoList;
	}
	
	/**
	 * Gets the photoList of the album
	 * @return photoList ArrayList<Photo>
	 */
	public ArrayList<Photo> getPhotoList() {
		return this.photoList;
	}
	
	/**
	 * removes the photo from the photolist
	 * @param photo Photo to remove
	 */
	public void deletePhoto(Photo photo) {
		this.photoList.remove(photo);
		this.items--;
	}
	
	/**
	 * Adds a photo to the photoList
	 * @param photo Photo to add to list
	 */
	public void addToList(Photo photo) {
		if(photoList == null) {
			photoList = new ArrayList<Photo>();
		}
		this.photoList.add(photo);
		this.items++;
	}
	
	/**
	 * Gets the Photo at the index
	 * @param i int Index
	 * @return photo Photo from photoList
	 */
	public Photo getPhoto(int i) {
		return this.photoList.get(i);
	}
	
	/**
	 * Gets the size of the photoList
	 * @return items Int
	 */
	public int getItems() {
	
		return this.items;
	}
	
	/**
	 * Increments the photoList size counter
	 */
	public void incrSize() {
		this.items++;
	}
	
	public void setName(String name) {
		this.name = (name);
	}
	
	/**
	 * Gets the name of the Album
	 * @return name String
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Sets the oldest Photo Date of the Album
	 * @param oldest Date to set to
	 */
	public void setOldestDate(Date oldest) {
		this.oldest = oldest;
	}
	
	/**
	 * Gets the Oldest Date in the album
	 * @return oldest Date
	 */
	public Date getOldestDate() {
		return this.oldest;
	}
	
	/**
	 * Sets the newest Date of the album
	 * @param newest Date to set to
	 */
	public void setNewestDate(Date newest) {
		this.newest = newest;
	}
	
	/**
	 * Gets the Newest date in the album
	 * @return newest Date
	 */
	public Date getNewestDate() {
		return this.newest;
	}
	
	

}
