package object;

/**
 * Photo Object
 * @author Daniel Kim
 * @author George Ding
 */
import java.io.*;
import java.util.ArrayList;
import java.util.Date;


import javafx.scene.image.Image;

public class Photo implements Serializable{

	private static final long serialVersionUID = 7568951594012126984L;
	
	private String caption = "Default Caption";
	public ArrayList<Tag> tagList;
	private Date date;
	private String path;
	private String name;
	private transient Image image;
	private transient Image thumbnail;
	
	/**
	 * Constructor for a new Photo
	 * @param name String
	 * @param path String
	 * @param date Date
	 */
	public Photo(String name, String path, Date date) {
		this.path = path;
		this.date = date;
		this.name = name;
		
		this.image = new Image("file:" + path, 513, 307, true, false);
		this.thumbnail = new Image("file:"+path, 50, 50, true, false);
		this.tagList = new ArrayList<Tag>();
	}
	
	/**
	 * Loads in the Stock photos of the Photo in Stock Photo folder
	 */
	public void setStock()
	{
		java.net.URL url = Photo.class.getResource(this.path);
		this.image = new Image(url.toString(), 513, 307, true, false);
		this.thumbnail = new Image(url.toString(), 50, 50, true, false);
	}
	
	/**
	 * Gets the Image of the Photo
	 * @return image Image
	 */
	public Image getImage() {
		return this.image;
	}
	
	/**
	 * Sets the image of the Photo based on the file path.
	 * Called during deserialization
	 */
	public void setImage() {
		this.image = new Image("file:" + this.path, 513, 307, true, false);
	}
	
	/**
	 * Gets the name of the photo
	 * @return name String
	 */
	public String getName() {
		return this.name;
	}
	/**
	 * Sets the name of the Photo
	 * @param name String
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Sets the Thumbnail of the Photo based on the file path.
	 * Called during deserialization
	 */
	public void setThumbnail() {
		this.thumbnail = new Image("file:" + this.path, 50, 50, true, false);
	}
	
	/**
	 * Gets the Thumbnail of the iamge
	 * @return thumbnail Image
	 */
	public Image getThumbnail() {
		return thumbnail;
	}
	
	/**
	 * sets the Caption of the Photo
	 * @param caption String
	 */
	public void caption(String caption) {
		this.caption = caption;
	}
	
	/**
	 * Gets the caption of the Photo
	 * @return caption String
	 */
	public String getCaption() {
		return this.caption;
	}
	
	/**
	 * Sets the Date of the photo
	 * 
	 * @param date Date
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 * Gets the Date of the photo
	 * @return date Date
	 */
	public Date getDate() {
		return this.date;
	}
	
	/**
	 * Sets the path of the Photo
	 * @param path String
	 */
	public void setPath(String path) {
		this.path = path;
	}
	
	/**
	 * Gets the path of the image
	 * @return path String
	 */
	public String getPath() {
		return this.path;
	}

	
	/**
	 * Gets the TagList of the Photo
	 * @return tagList ArrayList<Tag>
	 */
	public ArrayList<Tag> getTagList(){
		return this.tagList;
	}
	
	/**
	 * Sets the Tag List
	 * @param tagList ArrayList<Tag>
	 */ 
	public void setTagList(ArrayList<Tag> tagList) {
		this.tagList = tagList;
	}
}
