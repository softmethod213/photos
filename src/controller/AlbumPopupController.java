package controller;

/**
 * Popup which lists available albums for user
 * @author Daniel Kim
 * @author George Ding
 */
import java.util.Date;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import object.Album;
import object.Photo;

public class AlbumPopupController {
	
	private static Photo photo;
	private static Stage thisStage;
	private static String caller;
	private static Album currAlbum;
	private static ObservableList<Photo> photoList;
	
	@FXML ListView<Album> albumListView;
	
	/**
	 * Start method for Album popup. Sets the caller for which menuItem called
	 * the method (move or copy can call this) and does the action based on
	 * caller, album selected and the photo selected.
	 * @param stage Album popup stage
	 * @param caller Menu item that called this method
	 * @param albumList Albums available to the user
	 * @param obsPhotoList The ObservableList of photos in the current album
	 * @param album The current album
	 * @param photo The photo selected to move/copy
	 */
	public void start(Stage stage, String caller, ObservableList<Album> albumList, 
						ObservableList<Photo> obsPhotoList, Album album, Photo photo) {
		if(photo == null) {
			
			//no photo selected
			thisStage.close();
		}
		
		thisStage = stage;
		AlbumPopupController.caller = caller;
		currAlbum = album;
		AlbumPopupController.photo = photo;
		photoList = obsPhotoList;
		
		albumListView.setItems(albumList);
		
		albumListView.setCellFactory(param -> new ListCell<Album>() {
			
			@Override
			public void updateItem(Album a, boolean empty) {
				super.updateItem(a, empty);
				if(empty) {
					setText(null);
				}else {
					setText(a.getName());
				}
			}
		});
	}
	
	/**
	 * Event handler for choosing an album from the ListView. If
	 * the caller is move, the photo selected is deleted from the
	 * current album and copied into the selected album. Copy will
	 * just copy the photo into the selected album.
	 * 
	 * @param e MouseEvent for choosing an album within the listview.
	 */
	@FXML
	public void chooseAlbum(MouseEvent e) {
		Album album = albumListView.getSelectionModel().getSelectedItem();
		album.addToList(photo);
		if(caller.equals("move")) {
			currAlbum.deletePhoto(photo);
			photoList.remove(photo);
		}
		if(currAlbum.getPhotoList() != null && currAlbum.getPhotoList().size()>0) {
			Date oldest = currAlbum.getPhoto(0).getDate();
			Date newest = currAlbum.getPhoto(0).getDate();
			for(int i=1; i<currAlbum.getItems(); i++) {
				Date date = currAlbum.getPhoto(i).getDate();
				if(date.compareTo(newest) > 0) {
					newest = date;
				}
				if(date.compareTo(oldest) < 0) {
					oldest = date;
				}
			}
			currAlbum.setNewestDate(newest);
			currAlbum.setOldestDate(oldest);
		}
		if(album.getPhotoList() != null && album.getPhotoList().size()>0) {
			Date oldest = album.getPhoto(0).getDate();
			Date newest = album.getPhoto(0).getDate();
			for(int i=1; i<album.getItems(); i++) {
				Date date = album.getPhoto(i).getDate();
				if(date.compareTo(newest) > 0) {
					newest = date;
				}
				if(date.compareTo(oldest) < 0) {
					oldest = date;
				}
			}
			album.setNewestDate(newest);
			album.setOldestDate(oldest);
		}
		thisStage.close();
	}
}
