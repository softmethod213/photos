package controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import object.Photo;
import object.Tag;


/**
 * Controller that handles tagScreen.fxml.
 * @author Daniel Kim
 * @author George Ding
 *
 */
public class TagController {
	
	/**
	 * PhotoController instance that opened the Tag Controller
	 */
	private static PhotoController pc;
	
	/**
	 * Selected <b> Photo </b> instance that was highlighted in PhotoController 
	 */
	private Photo current;
	
	/**
	 * An Observable List of Tags that serves as container for ListView of Tags
	 */
	private static ObservableList<Tag> obsListTag;
	
	/**
	 * Main <b>AnchorPane</b> for tagScreen.fxml
	 */
	@FXML AnchorPane tagScreen;

	/**
	 * Viewable list of Tags to user for tagScreen.fxml
	 */
	@FXML ListView<Tag> tagListView;
	
	/**
	 * User input for Description of Tag Type "location"
	 */
	@FXML TextField locationTag;
	
	/**
	 * User input for Description of Tag Type "people"
	 */
	@FXML TextField peopleTag;
	
	/**
	 * User input for Generic Tag's Type
	 */
	@FXML TextField tagType;
	
	/**
	 * User input for Generic Tag's Description
	 */
	@FXML TextField tagDescription;
	
	/**
	 * User input for Description of Tag Type "date"
	 */
	@FXML TextField dateTag;
	
	/**
	 * Button that adds a location Tag
	 */
	@FXML Button addlocationTag;
	
	/**
	 * Button that adds a date Tag
	 */
	@FXML Button addDateTag;
	
	/**
	 * Button that adds a person Tag
	 */
	@FXML Button addpeopleTag;
	
	/**
	 * Button that adds a new Generic Tag
	 */
	@FXML Button newTagButton;
	
	/**
	 * Button that deletes selected Tag in <b>tagListView</b>
	 */
	@FXML Button deleteTag;
	
	
	/**
	 * Takes <b>current</b> Photo and loads it's container of <b>Tag</b>.
	 * @param current Current <b>Photo</b> selected.
	 * @param pc Instance of <b>PhotoController</b> that opened tagScreen.fxml.
	 */
	public void start(Photo current, PhotoController pc){
		this.current = current;
		TagController.pc = pc;
		
		if(current.getTagList() == null) {
			ArrayList<Tag> empty = new ArrayList<Tag>();
			current.setTagList(empty);
		}
		else {
			obsListTag = FXCollections.observableArrayList(current.getTagList());
			tagListView.setItems(obsListTag);
			
			tagListView.setCellFactory(param -> new ListCell<Tag>() {
				@Override
				protected void updateItem(Tag item, boolean empty) {
	                super.updateItem(item, empty);

	                if (empty || item == null ) {
	                    setText(null);
	                } else {
	                    setText(item.gettagType() + ": " + item.gettagInfo());
	                }
	            }
				
			});
		}

	}
	
	/**
	 * Deletes the selected Tag in <b>tagListView</b>.
	 * @param e ActionEvent when <b>deleteTag</b> button is pressed.
	 */
	@FXML
	public void delTag(ActionEvent e){
		Tag current = tagListView.getSelectionModel().getSelectedItem();
		if(current == null){
			Alert emptyTag = new Alert(Alert.AlertType.ERROR);
			emptyTag.setContentText("Please select a Tag");
			emptyTag.show();
			return;
		}
		else{
			this.current.tagList.remove(current);		
			obsListTag.remove(current);
			tagListView.setItems(obsListTag);
			
			tagListView.setCellFactory(param -> new ListCell<Tag>() {
				@Override
				protected void updateItem(Tag item, boolean empty) {
	                super.updateItem(item, empty);

	                if (empty || item == null ) {
	                    setText(null);
	                } else {
	                    setText(item.gettagType() + ": " + item.gettagInfo());
	                }
	            }
				
			});
			
			return;
		}
	}
	
	/**
	 * Adds a generic <b>Tag</b> of type "location" and user specified tag description.
	 * Only one allowed per photo.
	 * @param e ActionEvent when <b>addlocationTag</b> Button is pressed.
	 */
	@FXML
	public void addlTag(ActionEvent e) {
		if(locationTag.getText().equals("")) {
			Alert empty = new Alert(Alert.AlertType.ERROR);
			empty.setContentText("Empty Location TextField");
			empty.show();
			return;
		}
		else {
			for(int i = 0; i < current.tagList.size(); i++) {
				if(current.tagList.get(i).gettagType().equals("location")) {
					Alert location = new Alert(Alert.AlertType.ERROR);
					location.setContentText("Only 1 Location Tag Allowed");
					location.show();
					return;
				}
			}
			Tag newTag = new Tag("location", locationTag.getText().trim().toLowerCase());
			this.current.tagList.add(newTag);
			locationTag.clear();

			Comparator<Tag> newComp = new Comparator<Tag>() {

				@Override
				public int compare(Tag arg0, Tag arg1) {
					if(arg0.gettagType().equals(arg1.gettagType())) {
						return arg0.gettagInfo().compareTo(arg1.gettagInfo());
					}
					else {
						return arg0.gettagType().compareTo(arg1.gettagType());
					}
				}
				
			};
			
			Collections.sort(this.current.tagList, newComp);
			
			obsListTag = FXCollections.observableArrayList(this.current.tagList);
			tagListView.setItems(obsListTag);
			
			tagListView.setCellFactory(param -> new ListCell<Tag>() {
				@Override
				protected void updateItem(Tag item, boolean empty) {
	                super.updateItem(item, empty);

	                if (empty || item == null ) {
	                    setText(null);
	                } else {
	                    setText(item.gettagType() + ": " + item.gettagInfo());
	                }
	            }
				
			});
			return;
		}
	}
	
	/**
	 * Adds a generic <b>Tag</b> of type "person" and user specified tag description.
	 * Multiple people tag allowed.
	 * @param e ctionEvent when <b>addpeopleTag</b> Button is pressed. 
	 */
	@FXML
	public void addpTag(ActionEvent e) {
		if(peopleTag.getText().equals("")) {
			Alert empty = new Alert(Alert.AlertType.ERROR);
			empty.setContentText("Empty People TextField");
			empty.show();
			return;
		}
		else {
			Tag newTag = new Tag("person", peopleTag.getText().trim().toLowerCase());
			this.current.tagList.add(newTag);
			peopleTag.clear();

			Comparator<Tag> newComp = new Comparator<Tag>() {

				@Override
				public int compare(Tag arg0, Tag arg1) {
					if(arg0.gettagType().equals(arg1.gettagType())) {
						return arg0.gettagInfo().compareTo(arg1.gettagInfo());
					}
					else {
						return arg0.gettagType().compareTo(arg1.gettagType());
					}
				}
				
			};
			
			Collections.sort(this.current.tagList, newComp);
			
			obsListTag = FXCollections.observableArrayList(this.current.tagList);
			tagListView.setItems(obsListTag);
			
			tagListView.setCellFactory(param -> new ListCell<Tag>() {
				@Override
				protected void updateItem(Tag item, boolean empty) {
	                super.updateItem(item, empty);

	                if (empty || item == null ) {
	                    setText(null);
	                } else {
	                    setText(item.gettagType() + ": " + item.gettagInfo());
	                }
	            }
				
			});
			return;
		}
	}
	
	/**
	 * Adds a <b>Tag</b> of type "Date" and user specified description.
	 * Only one Date Tag allowed per photo.
	 * @param e ActionEvent when <b>addDateTag</b> Button is pressed.
	 */
	@FXML 
	public void addDTag(ActionEvent e) {
		if(dateTag.getText().equals("")) {
			Alert empty = new Alert(Alert.AlertType.ERROR);
			empty.setContentText("Empty Date TextField");
			empty.show();
			return;
		}
		else {
			for(int i = 0; i < current.tagList.size(); i++) {
				if(current.tagList.get(i).gettagType().equals("date")) {
					Alert location = new Alert(Alert.AlertType.ERROR);
					location.setContentText("Only 1 Date Tag Allowed");
					location.show();
					return;
				}
			}
			Tag newTag = new Tag("date", dateTag.getText().trim().toLowerCase());
			this.current.tagList.add(newTag);
			dateTag.clear();

			Comparator<Tag> newComp = new Comparator<Tag>() {

				@Override
				public int compare(Tag arg0, Tag arg1) {
					if(arg0.gettagType().equals(arg1.gettagType())) {
						return arg0.gettagInfo().compareTo(arg1.gettagInfo());
					}
					else {
						return arg0.gettagType().compareTo(arg1.gettagType());
					}
				}
				
			};
			
			Collections.sort(this.current.tagList, newComp);
			
			obsListTag = FXCollections.observableArrayList(this.current.tagList);
			tagListView.setItems(obsListTag);
			
			tagListView.setCellFactory(param -> new ListCell<Tag>() {
				@Override
				protected void updateItem(Tag item, boolean empty) {
	                super.updateItem(item, empty);

	                if (empty || item == null ) {
	                    setText(null);
	                } else {
	                    setText(item.gettagType() + ": " + item.gettagInfo());
	                }
	            }
				
			});
			return;
		}
	}
	
	/**
	 * Adds a new <b>Tag</b> of user specified type and description.
	 * @param e ActionEvent when <b>newTagButton</b> Button is pressed
	 */
	@FXML 
	public void addTag(ActionEvent e){
		if(tagType.getText().isEmpty() || tagDescription.getText().isEmpty()){
			Alert emptyText = new Alert(Alert.AlertType.ERROR);
			emptyText.setContentText("TextField cannot be empty");
			emptyText.show();
			tagType.clear();
			tagDescription.clear();
			return;
		}
		else{
			if(tagType.getText().trim().toLowerCase().equals("date")) {
				for(int i = 0; i < this.current.getTagList().size(); i++) {
					if(this.current.getTagList().get(i).gettagType().equals("date")) {
						Alert existing = new Alert(Alert.AlertType.ERROR);
						existing.setContentText("Only 1 Date tag allowed");
						existing.show();
						return;
					}
				}
			}
			else if(tagType.getText().trim().toLowerCase().equals("location")) {
				for(int i = 0; i < this.current.getTagList().size(); i++) {
					if(this.current.getTagList().get(i).gettagType().equals("location")) {
						Alert existing = new Alert(Alert.AlertType.ERROR);
						existing.setContentText("Only 1 location tag allowed");
						existing.show();
						return;
					}
				}
			}
			
			Tag newTag = new Tag(tagType.getText().trim().toLowerCase(), tagDescription.getText().trim().toLowerCase());
			this.current.tagList.add(newTag);
			tagType.clear();
			tagDescription.clear();

			Comparator<Tag> newComp = new Comparator<Tag>() {

				@Override
				public int compare(Tag arg0, Tag arg1) {
					if(arg0.gettagType().equals(arg1.gettagType())) {
						return arg0.gettagInfo().compareTo(arg1.gettagInfo());
					}
					else {
						return arg0.gettagType().compareTo(arg1.gettagType());
					}
				}
				
			};
			
			Collections.sort(this.current.tagList, newComp);
			
			obsListTag = FXCollections.observableArrayList(this.current.tagList);
			tagListView.setItems(obsListTag);
			
			tagListView.setCellFactory(param -> new ListCell<Tag>() {
				@Override
				protected void updateItem(Tag item, boolean empty) {
	                super.updateItem(item, empty);

	                if (empty || item == null ) {
	                    setText(null);
	                } else {
	                    setText(item.gettagType() + ": " + item.gettagInfo());
	                }
	            }
				
			});
			return;
		}
	}
}
