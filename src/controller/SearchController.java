package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import controller.AlbumController;
import object.Album;
import object.Photo;
import object.Tag;

/**
 * Controller that handles FindPopup.fxml
 * @author Daniel Kim
 * @author George Ding
 *
 */
public class SearchController {

	private static Stage stage;
	
	private static boolean context = false;
	
	private static ArrayList<Photo> resultList;
	
	
	
	@FXML MenuItem dateDropDown;
	@FXML Text minText;
	@FXML Text maxText;
	@FXML TextField minTextField;
	@FXML TextField maxTextField;
	
	@FXML MenuItem tagDropDown;
	@FXML Text tagText;
	@FXML TextField tagTextField;
	
	@FXML Button searchButton;
	
	/**
	 * Start method for Search Screen.
	 * @param stage 
	 * @param albumStage
	 * @param photoScreen
	 * @param asc
	 */
	public void start(Stage stage, Stage albumStage, AnchorPane photoScreen,
				AlbumController asc) {
		
		SearchController.stage = stage;
		searchButton.setDisable(true);
	}
	
	/**Sets the Date TextField for Search to be transparent 
	 * @param e ActionEvent that clears the text
	 */
	@FXML
	public void showDateSearch(ActionEvent e) {
		tagText.setOpacity(0);
		tagTextField.setOpacity(0);
		tagTextField.setDisable(true);
		tagTextField.clear();
		
		minText.setOpacity(100);
		maxText.setOpacity(100);
		minTextField.setOpacity(100);
		maxTextField.setOpacity(100);
		searchButton.setOpacity(100);
		searchButton.setDisable(false);
		context = true;
	}
	
	/**Sets the Tag TextField for Search to be transparent 
	 * @param e ActionEvent that clears the text
	 */
	@FXML
	public void showTagSearch(ActionEvent e) {
		minText.setOpacity(0);
		maxText.setOpacity(0);
		minTextField.setOpacity(0);
		minTextField.clear();
		maxTextField.setOpacity(0);
		maxTextField.clear();
		
		tagText.setOpacity(100);
		tagTextField.setOpacity(100);
		tagTextField.setDisable(false);
		searchButton.setOpacity(100);
		searchButton.setDisable(false);
		context = false;
	}
	
	/**
	 * Searches for photos in all albums by date or tags.
	 * @param e ActionEvent when <b>searchButton</b> is pressed
	 * @throws ParseException Throws Exception when date is incorrect Format
	 */
	@FXML
	public void search(ActionEvent e) throws ParseException{
		
		if(context == true) {
			//search by date
			
			//implement here
			if(minTextField == null || maxTextField == null) {
				Alert emptyFields = new Alert(Alert.AlertType.ERROR);
				emptyFields.setContentText("Fields cannot be empty");
				emptyFields.show();
			}else {
				
				ArrayList<Photo> photoList = new ArrayList<Photo>();
				
				//implement here
				if(!minTextField.getText().contains("/") || !maxTextField.getText().contains("/")){
					Alert alert = new Alert(Alert.AlertType.ERROR);
					alert.setContentText("Please format Correctly");
					alert.show();
					return;
				}
				String pattern = "MM/dd/yyyy";
				SimpleDateFormat smf = new  SimpleDateFormat(pattern);
				
				Date minDate = new Date();
				Date maxDate = new Date();
				
				try{
					minDate = smf.parse(minTextField.getText().trim());
					maxDate = smf.parse(maxTextField.getText().trim());
				}
				catch(ParseException pExcept){
					Alert wrongParse = new Alert(Alert.AlertType.ERROR);
					wrongParse.setContentText("Incorrect Formatting");
					wrongParse.show();
				}
				
				for(int i = 0; i < AlbumController.albumArrList.size(); i++){
					Album currentAlb = AlbumController.albumArrList.get(i);
					
					for(int j = 0; j < currentAlb.getPhotoList().size(); j++){
						Photo currentPhoto = currentAlb.getPhotoList().get(i);
						Date photoDate = currentPhoto.getDate();
						
						if((photoDate.after(minDate) && photoDate.before(maxDate)) || (photoDate.equals(minDate)) || (photoDate.equals(maxDate))){
							photoList.add(currentPhoto);
							break;
						}
					}
				}
				
				resultList = photoList;
				
				//dont't touch
				minText.setOpacity(0);
				maxText.setOpacity(0);
				minTextField.setOpacity(0);
				minTextField.clear();
				maxTextField.setOpacity(0);
				maxTextField.setOpacity(0);
				searchButton.setOpacity(0);
				searchButton.setDisable(true);
			}
		}else {
			//search by Tag
			
			//implement here
			if(tagTextField == null) {
				Alert empty = new Alert(Alert.AlertType.ERROR);
				empty.setContentText("Empty TextField");
				empty.show();
			}else {
				
				//implement here
				
				ArrayList<Photo> photoList = new ArrayList<Photo>();
				
				String parts[] = tagTextField.getText().split(" ");
				
				System.out.println(parts[0]);
				
				if(parts.length == 1){
					String tagParts[] = parts[0].split("=");
					
					for(int i = 0; i < AlbumController.albumArrList.size(); i++){
						Album currentAlb = AlbumController.albumArrList.get(i);
						
						for(int j = 0; j < currentAlb.getPhotoList().size(); j++){
							Photo currentPhoto = currentAlb.getPhotoList().get(j);
							
							for(int k = 0; k < currentPhoto.tagList.size(); k++){
								Tag currentTag = currentPhoto.tagList.get(k);
								
								if(currentTag.gettagInfo().equals(tagParts[1].trim().toLowerCase()) && currentTag.gettagType().equals(tagParts[0].trim().toLowerCase())){
									photoList.add(currentPhoto);
									break; 	
								}
							}
						}
					}
				}
				else if(parts.length == 3){
					boolean first = false;
					boolean second = false;
					String firstTagParts[] = parts[0].split("=");
					String conditional = parts[1];
					String secondTagParts[] = parts[2].split("=");
					
					if(conditional.trim().toLowerCase().equals("or")){
						for(int i = 0; i < AlbumController.albumArrList.size(); i++){
							Album currentAlb = AlbumController.albumArrList.get(i);
							
							for(int j = 0; j < currentAlb.getPhotoList().size(); j++){
								Photo currentPhoto = currentAlb.getPhotoList().get(j);
								
								for(int k = 0; k < currentPhoto.tagList.size(); k++){
									Tag currentTag = currentPhoto.tagList.get(k);
									
									if(currentTag.gettagInfo().equals(firstTagParts[1].trim().toLowerCase()) && currentTag.gettagType().equals(firstTagParts[0].trim().toLowerCase())){
										System.out.println(currentPhoto.getName() + " added");
										photoList.add(currentPhoto);
										break;
									}
									else if(currentTag.gettagInfo().equals(secondTagParts[1].trim().toLowerCase()) && currentTag.gettagType().equals(secondTagParts[0].trim().toLowerCase())){
										System.out.println(currentPhoto.getName() + " added");
										photoList.add(currentPhoto);
										break;
									}
								}
							}
						}	
					}
					else if(conditional.trim().toLowerCase().equals("and")){
						for(int i = 0; i < AlbumController.albumArrList.size(); i++){
							Album currentAlb = AlbumController.albumArrList.get(i);
							
							for(int j = 0; j < currentAlb.getPhotoList().size(); j++){
								Photo currentPhoto = currentAlb.getPhotoList().get(j);
								
								for(int k = 0; k < currentPhoto.tagList.size(); k++){
									Tag currentTag = currentPhoto.tagList.get(k);
									
									if(currentTag.gettagInfo().equals(firstTagParts[1].trim().toLowerCase()) && currentTag.gettagType().equals(firstTagParts[0].trim().toLowerCase())){
										first = true;
									}
									if(currentTag.gettagInfo().equals(secondTagParts[1].trim().toLowerCase()) && currentTag.gettagType().equals(secondTagParts[0].trim().toLowerCase()))
										second = true;
								}
								
								if(first && second){
									System.out.println(currentPhoto.getName() + " is added");
									photoList.add(currentPhoto);
								}
								
								first = false;
								second = false;
							}
						}
					}
				}
				else{
					Alert alert = new Alert(Alert.AlertType.ERROR);
					alert.setContentText("Format not recognized");
					alert.show();
				}
				
				resultList = photoList;
				
				//don't touch
				tagText.setOpacity(0);
				tagTextField.setOpacity(0);
				tagTextField.clear();
				searchButton.setOpacity(0);
				searchButton.setDisable(true);
			}
		}
		stage.close();
	}
	
	/**
	 * Gets the finalized list of Photos in search by either Tags or Date.
	 * @return resultList - ArrayList of Photo's
	 */
	public static ArrayList<Photo> getList(){
		return resultList;
	}
}
