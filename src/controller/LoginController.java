package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.Scanner;

import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Contoller for Login Screen
 * @author Daniel Kim
 * @author George Ding
 *
 */
public class LoginController {
	
	/**
	 * The loginStage to go back to from this stage.
	 */
	public static Stage loginStage;
	private static File loginFile;

	@FXML private static AnchorPane albumScreen;
	@FXML private static AnchorPane registrationScreen;
	@FXML private static VBox adminListScreen;
	
	@FXML TextField userTextField;
	@FXML Button loginButton;
	@FXML Hyperlink registerLink;
	
	/**
	 * Start method for login controller. Sets the loginStage and login file
	 * to read from.
	 * @param loginStage
	 * @param loginFile
	 */
	public void start(Stage loginStage, File loginFile) {
		LoginController.loginFile = loginFile;
		LoginController.loginStage = loginStage;
	}
	
	/**
	 * Login method called by loginKeyPress() and login().
	 * Check for username data and opens the album stage.
	 */
	public void loginMethod() {
		String user = userTextField.getText().trim();

		if(user == null) {
			Alert noUser = new Alert(Alert.AlertType.ERROR);
			noUser.setContentText("No username entered");
			noUser.show();
		}
		
		Scanner scan = null;
		try{
			scan = new Scanner(loginFile);
		}catch(FileNotFoundException e1) {
			//shouldnt happen.
		}
		
		if(user.equals("admin")) {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/fxml/AdminListPopup.fxml"));
			
			try {
				adminListScreen = loader.load();
				
				AdminController alc = loader.getController();
				
				Stage stage = new Stage();
				
				alc.start(loginStage, stage, loginFile);
				
				userTextField.clear();
				
				loginStage.close();
				
				Scene scene = new Scene(adminListScreen);
				stage.setScene(scene);
				stage.setResizable(false);
				stage.show();
				
				
				return;
			}catch (IOException e1) {
				System.out.println(e1);
				//shouldn't happen
			}
		}
		
		while(scan.hasNextLine()) {
			String scanned = scan.nextLine().trim();

			if(scanned.equals(user)) {
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("/fxml/AlbumScreen.fxml"));
				
				try {
					
					albumScreen = (AnchorPane) loader.load();
					
					AlbumController asc = loader.getController();
					
					Stage stage = new Stage();
	
					asc.start(loginStage, stage, user);
					
					userTextField.clear();
					
					loginStage.close();
					
					Scene scene = new Scene(albumScreen);
					stage.setScene(scene);
					stage.setResizable(false);
					stage.show();
					
					return;
				}catch (IOException e1) {
					System.out.println(e1);
					Alert missingFXML = new Alert(Alert.AlertType.ERROR);
					missingFXML.setContentText("Error code:  Missing FXML.  Please debug.");
					missingFXML.show();
				}
			}
		}
		Alert NAuser = new Alert(Alert.AlertType.ERROR);
		NAuser.setContentText("User does not exist");
		NAuser.show();
		return;
	}
	
	/**
	 * EventHandler for enter key on login. Calls loginMethod()
	 * @param e KeyEvent for enter key to login
	 */
	@FXML
	public void loginKeyPress(KeyEvent e) {
		
		if(e.getCode().equals(KeyCode.ENTER)) {
			loginMethod();
		}
	}
	
	/**
	 * EventHandler for login button when pressed. Call loginMethod()
	 * @param e ActionEvent for login button
	 */
	@FXML
	public void login(ActionEvent e) {
		loginMethod();
	}
	
	/**
	 * Eventhandler for hyperlink. Opens signup popup.
	 * @param e MouseEvent for clicking the hyperlink.
	 */
	@FXML
	public void signup(MouseEvent e) {
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/fxml/RegistrationScreen.fxml"));
		
		
		
		try {
			
			registrationScreen = loader.load();
			
			RegistrationController rc = loader.getController();
			Stage stage = new Stage();

			rc.start(stage, loginFile);
			
			Scene scene = new Scene(registrationScreen);
			stage.setScene(scene);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setResizable(false);
			stage.show();
			
		}catch (Exception e1) {
			System.out.println(e);
		}
		
	}
	
}
