package controller;

/**
 * User registration class
 * @author Daniel Kim
 * @author George Ding
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class RegistrationController {

	private File loginFile;
	private Stage stage;
	
	@FXML TextField userTextField;
	
	/**
	 * Start method for the signup screen.
	 * @param stage
	 * @param loginFile
	 */
	public void start(Stage stage, File loginFile) {
		this.loginFile = loginFile;
		this.stage = stage;
	}
	
	/**
	 * Registers the user if the user does not exist. Writes to
	 * the login data file the username.
	 * @param e ActionEvent for the register button
	 * @throws IOException Exception thrown if the login data isn't found
	 */
	@FXML
	public void registerUser(ActionEvent e) throws IOException {
		
		String user = userTextField.getText();
		if(user == null) {
			Alert noUser = new Alert(Alert.AlertType.ERROR);
			noUser.setContentText("No username entered");
			noUser.show();
		}
		
		Scanner readUser = new Scanner(loginFile);
		while(readUser.hasNextLine()) {
			if(user.equals(readUser.nextLine())) {
				Alert userExists = new Alert(Alert.AlertType.ERROR);
				userExists.setContentText("User has already been registered");
				userExists.show();
				return;
			}
		}
		readUser.close();
		
		FileWriter fw = new FileWriter(loginFile, true);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.newLine();
		bw.write(user);
		bw.close();
		fw.close();
		
		//Dialog: Successfully registered
		
		stage.close();
	}
	
	/**
	 * Cancels the registration window
	 * @param e ActionEvent for the cancel button
	 */
	@FXML
	public void cancel(ActionEvent e) {
		stage.close();
	}
}
