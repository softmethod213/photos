package controller;

/**
 * Admin subsystem controller
 * @author Daniel Kim
 * @author George Ding
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class AdminController {

	private static Stage stage;
	private static Stage loginStage;
	private static File loginFile;
	
	private static ObservableList<String> userList;
	
	@FXML ListView<String> adminListView;
	@FXML Button addButton;
	@FXML Button deleteButton;
	@FXML Button logoutButton;
	@FXML TextField userTextField;
	
	/**
	 * Start call of the admin subsystem. Takes in the Login Stage so it knows
	 * what stage to go back to at admin logout. loginFile is the user data to read
	 * in.
	 * 
	 * @param loginStage Login Stage to go back to
	 * @param stage	Admin subsystem stage
	 * @param loginFile User data file
	 */
	public void start(Stage loginStage, Stage stage, File loginFile) {
		AdminController.loginStage = loginStage;
		AdminController.stage = stage;
		AdminController.loginFile = loginFile;
		
		Scanner scan = null;
		try {
			scan = new Scanner(loginFile);
		} catch (FileNotFoundException e1) {
			//shouldn't happen
			e1.printStackTrace();
		}
		
		ArrayList<String> list = new ArrayList<String>();
		while(scan.hasNextLine()) {
			list.add(scan.nextLine().trim());
		}
		
		userList = FXCollections.observableArrayList(list);
		adminListView.setItems(userList);
	}
	
	/**
	 * Adds the user typed in the textfield at press of add button
	 * 
	 * @param e ActionEvent of Button
	 */
	@FXML
	public void addUser(ActionEvent e) {
	
		String user = userTextField.getText().trim();
		
		for(int i=0; i<userList.size(); i++) {
			if(userList.get(i).equals(user)) {
				Alert existingUser = new Alert(Alert.AlertType.ERROR);
				existingUser.setContentText("User already Exists");
				existingUser.show();
				return;
			}
		}
		
		userList.add(user);
		userTextField.clear();
		
		FileWriter fw;
		try {
			fw = new FileWriter(loginFile, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.newLine();
			bw.write(user);
			bw.close();
			fw.close();
		}catch (IOException e1) {
			
			//shouldn't happen
			e1.printStackTrace();
		}
	}
	
	/**
	 * Deletes user at press of delete button based on username in textfield.
	 * @param e ActionEvent of Delete Button
	 */
	@FXML
	public void deleteUser(ActionEvent e) {
		
		String user = userTextField.getText().trim();
		
		for(int i=0; i<userList.size(); i++) {
			if(userList.get(i).equals(user)) {
				userList.remove(i);
				
				FileWriter fw;
				
				try {
					fw = new FileWriter(loginFile);
					BufferedWriter bw = new BufferedWriter(fw);
					for(int j=0; j<userList.size(); j++) {
						bw.write(userList.get(j));
						if(j != userList.size()-1) {
							bw.newLine();
						}
					}
					File file = new File("src/Photo Data/" + user + ".ser");
					if(file.exists()) {
						file.delete();
					}
					bw.close();
					fw.close();
				}catch(IOException e1) {
					//shouldnt happen
				}
				userTextField.clear();
				return;
			}
		}
		
		Alert NAuser = new Alert(Alert.AlertType.ERROR);
		NAuser.setContentText("User doesn't exist");
		NAuser.show();
	}
	
	/**
	 * Fills in the textField used for adding and deleting users.
	 * 
	 * @param e MouseEvent used for the selected item in the listview.
	 */
	@FXML
	public void fillTextField(MouseEvent e) {
		
		userTextField.setText(adminListView.getSelectionModel().getSelectedItem());
	}
	
	/**
	 * Logout action. Goes to the Login stage at press of logout button.
	 * 
	 * @param e ActionEvent for logout button.
	 */
	@FXML
	public void logout(ActionEvent e) {
		AdminController.loginStage.show();
		AdminController.stage.close();
	}
}
