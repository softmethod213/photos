package controller;

/**
 * Popup for renaming an album
 * @author Daniel Kim
 * @author George Ding
 */
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class RenameAlbumController {

	/**
	 * The popup stage for renaming the album (this stage)
	 */
	Stage stage;
	
	/**
	 * Album Controller for refreshing the album table
	 */
	AlbumController asc;
	
	@FXML TextField albumTextField;
	@FXML TextField newTextField;
	@FXML Button renameButton;
	
	/**
	 * Start method for Rename popup
	 * @param stage Stage opened for popup (this stage)
	 * @param asc AlbumController
	 */
	public void start(Stage stage, AlbumController asc) {
		this.stage = stage;
		this.asc = asc;
	}
	
	/**
	 * Renames the album from the old album name to the new album name.
	 */
	@FXML
	public void rename() {
		
		boolean exists = false;
		for(int i=0; i<AlbumController.albumList.size(); i++) {
			if(AlbumController.albumList.get(i).getName().equals(newTextField.getText())) {
				Alert existingAlbum = new Alert(Alert.AlertType.ERROR);
				existingAlbum.setContentText("Album already exists.");
				existingAlbum.show();
				
				exists = true;
			}
		}
		
		if(exists == false) {
			for(int i=0; i<AlbumController.albumList.size(); i++) {
				if(AlbumController.albumList.get(i).getName().equals(albumTextField.getText())) {
					AlbumController.albumList.get(i).setName(newTextField.getText());
					asc.albumTableView.refresh();
					AlbumController.albumArrList.get(i).setName(newTextField.getText());
					albumTextField.clear();
					newTextField.clear();
					stage.close();
					return;
				}
			}
		}
	}
}
