package controller;
/**
 * Photo Controller
 * @author Daniel Kim
 * @author George Ding
 */
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import object.Album;
import object.Photo;

public class PhotoController {
	
	private static Stage albumListStage;
	private static Stage thisStage;
	private static Album album;

	private static ObservableList<Photo> obsPhotoList;
	
	private static AlbumController asc;
	private static ArrayList<Photo> tempPhotoList;
	
	@FXML AnchorPane tagScreen;
	
	@FXML MenuItem backToAlbumMenuItem;
	@FXML MenuItem addMenuItem;
	@FXML MenuItem deleteMenuItem;
	@FXML MenuItem copyMenuItem;
	@FXML MenuItem moveMenuItem;
	@FXML MenuItem newAlbumMenuItem;
	@FXML MenuItem logoutMenuItem;
	@FXML MenuItem editMenuCaption;
	
	@FXML ListView<Photo> thumbnailListView;
	@FXML AnchorPane albumPopup;

	
	@FXML MenuItem captionMenuItem;
	@FXML MenuItem tagMenuItem;

	@FXML Text tags;
	@FXML Text captionTextField;
	@FXML Text tagTextField;
	@FXML TextField captionText;
	
	@FXML Text dateText;
	@FXML Text dateTextField;
	
	@FXML Text nameText;
	@FXML Text pathText;
	@FXML Text dateModText;

	@FXML ImageView imageViewer;
	@FXML Button nextButton;
	@FXML Button prevButton;
	
	/**
	 * Normal Start for opening photo screen (clicking the album)
	 * @param albumListStage Album stage to go back to
	 * @param thisStage Photo stage
	 * @param album The current album opened
	 * @param asc The album screen controller
	 */
	public void normStart(Stage albumListStage, Stage thisStage, Album album, AlbumController asc) {
		PhotoController.thisStage = thisStage;
		PhotoController.albumListStage = albumListStage;
		PhotoController.album = album;
		PhotoController.asc = asc;
		
		//*
		obsPhotoList = FXCollections.observableArrayList(album.getPhotoList());
		
		thumbnailListView.setItems(obsPhotoList);
		
		newAlbumMenuItem.setDisable(true);
	
		thumbnailListView.setCellFactory(param -> new ListCell<Photo>() {
			
			
			@Override
			public void updateItem(Photo p, boolean empty) {
				super.updateItem(p, empty);
				if(empty) {
					setText(null);
					setGraphic(null);
				}else {
					
					setText(p.getCaption());
					setGraphic(new ImageView(p.getThumbnail()));
				}
			}
		});//*/
	}
	
	/**
	 * Opens a photo screen with the search results.
	 * @param albumListStage Album stage to go back to
	 * @param thisStage The current Photo stage
	 * @param asc Album screen Controller
	 * @param tempPhotoList The list of photos from the search result
	 */
	public void searchedStart(Stage albumListStage, Stage thisStage, 
				AlbumController asc, ArrayList<Photo> tempPhotoList) {
		PhotoController.thisStage = thisStage;
		PhotoController.albumListStage = albumListStage;
		PhotoController.asc = asc;
		PhotoController.tempPhotoList = tempPhotoList;
		
		obsPhotoList = FXCollections.observableArrayList(tempPhotoList);
		thumbnailListView.setItems(obsPhotoList);

		thumbnailListView.setCellFactory(param -> new ListCell<Photo>() {
			
			@Override
			public void updateItem(Photo p, boolean empty) {
				super.updateItem(p, empty);
				if(empty) {
					setText(null);
					setGraphic(null);
				}else {
					
					setText(p.getCaption());
					setGraphic(new ImageView(p.getThumbnail()));
				}
			}
		});
	}
	
	/**
	 * Go back to the Album list of the user. Calls backToAlbum()
	 * @param e ActionEvent for the MenuItem to go back to Album list
	 */
	@FXML
	public void backToAlbums(ActionEvent e) {
		backToAlbum();
		albumListStage.show();
		asc.albumTableView.refresh();
		thisStage.close();
	}
	
	/**
	 * Static method for going back to the Album list
	 * Refreshes the table of albums and sets the Dates newest and oldest
	 */
	public static void backToAlbum() {
		if(album == null) {
			//leave empty. i dont feel like making try catch
		}else
		if(album.getPhotoList().size()<1) {
			album.setNewestDate(null);
			album.setOldestDate(null);
		}else
		
		if(album.getPhotoList() != null && album.getPhotoList().size()>0) {
			Date oldest = album.getPhoto(0).getDate();
			Date newest = album.getPhoto(0).getDate();
			for(int i=1; i<album.getItems(); i++) {
				Date date = album.getPhoto(i).getDate();
				if(date.compareTo(newest) > 0) {
					newest = date;
				}
				if(date.compareTo(oldest) < 0) {
					oldest = date;
				}
			}
			album.setNewestDate(newest);
			album.setOldestDate(oldest);
		}
	}
	
	/**
	 * Adds photo to the album. Opens the filechooser
	 * and creates a Photo object which is then put in the
	 * photoList and the observable photo list
	 * @param e ActionEvent for MenuItem to add a photo to the album
	 */
	@FXML
	public void addToAlbum(ActionEvent e) {
		
		/*
		if(AlbumController.user.equals("stock")) {
			Photo photo1 = new Photo("haruhi.png",
					"/Stock Photos/haruhi.png",
					new Date());
			
			album.addToList(photo1);
			obsPhotoList.add(photo1);
			
			Photo photo2 = new Photo("hehe.jpg",
					"/Stock Photos/hehe.jpg",
					new Date());
			
			album.addToList(photo2);
			obsPhotoList.add(photo2);
			
			Photo photo3 = new Photo("images.png",
					"/Stock Photos/images.png",
					new Date());
			
			album.addToList(photo3);
			obsPhotoList.add(photo3);
		
			Photo photo4 = new Photo("index.png",
					"/Stock Photos/index.png",
					new Date());
			
			album.addToList(photo4);
			obsPhotoList.add(photo4);
			
			Photo photo5 = new Photo("scarlet_knight_logo_hp.png",
					"/Stock Photos/scarlet_knight_logo_hp.png",
					new Date());
			
			album.addToList(photo5);
			obsPhotoList.add(photo5);
			return;
		}
		//*/
		FileChooser fc = new FileChooser();
		fc.setInitialDirectory(new File(System.getProperty("user.home")));  
		fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("PNG", "*.png"),
				new FileChooser.ExtensionFilter("JPG", "*.jpg"));
		File chosen = fc.showOpenDialog(thisStage);
		
		if(chosen == null) {
			return;
		}
		
		Photo photo = new Photo(chosen.getName(), chosen.getPath(), new Date(chosen.lastModified()));
		album.addToList(photo);
		
		obsPhotoList.add(photo);
	}
	
	/**
	 * EventHandler for deleting a photo from the album. Deletes the selected
	 * photo in the listview.
	 * @param e ActionEvent for the MenuItem to delete a photo from the list.
	 */
	@FXML
	public void deletePhoto(ActionEvent e) {
		Photo p = thumbnailListView.getSelectionModel().getSelectedItem();
		obsPhotoList.remove(p);
		album.deletePhoto(p);
	}
		
	/**
	 * EventHandler for copying a photo from the current album to the
	 * chosen album. Opens a popup with a list of the user's albums.
	 * @param e ActionEvent for MenuItem to copy a photo
	 */
	@FXML
	public void copyPhoto(ActionEvent e) {
		Photo p = thumbnailListView.getSelectionModel().getSelectedItem();
		
		if(p == null) {
			//Error: no picture selected
			return;
		}
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/fxml/AlbumListView.fxml"));
		
		try {
			albumPopup = (AnchorPane) loader.load();
			
			AlbumPopupController apc = loader.getController();
			
			Stage stage = new Stage();
			
			apc.start(stage, "copy", asc.albumTableView.getItems(),
					obsPhotoList, album, p);
			
			Scene scene = new Scene(albumPopup);
			stage.setScene(scene);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
		}catch(IOException e1) {
			System.out.println(e1);
		}
	}
	
	/**
	 * EventHandler to move an album to another album. Opens a popup
	 * of the user's album list and moves it to the selected album.
	 * @param e ActionEvent for the MenuItem to move a photo
	 */
	@FXML
	public void moveToAlbum(ActionEvent e) {
		Photo p = thumbnailListView.getSelectionModel().getSelectedItem();
		
		if(p == null) {
			//Error: no picture selected
			return;
		}
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/fxml/AlbumListView.fxml"));
		
		try {
			albumPopup = (AnchorPane) loader.load();
			
			AlbumPopupController apc = loader.getController();
			
			Stage stage = new Stage();
			
			apc.start(stage, "move", asc.albumTableView.getItems(),
					obsPhotoList , album, p);
			
			Scene scene = new Scene(albumPopup);
			stage.setScene(scene);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
		}catch(IOException e1) {
			System.out.println(e1);
		}
	}
	
	/**
	 * Create an Album with the photos from the search results. Names
	 * the album "New Album"+the date the album was created.
	 * @param e ActionEvent for the MenuItem to create a new album with the search results
	 */
	@FXML
	public void createResultsAlbum(ActionEvent e) {
		Album album = new Album("New Album" + new Date(), tempPhotoList);
		AlbumController.albumList.add(album);
		AlbumController.albumArrList.add(album);
		asc.albumTableView.refresh();
		
		PhotoController.album = album;
		
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setContentText(album.getName() + "successfully created");
		alert.show();
	}
	
	/**
	 * EventHandler to edit a photo's caption. Uses the textfield in the menuitem
	 * to determine what it will put in as the caption.
	 * @param e ActionEvent for the MenuItem to edit the caption
	 */
	@FXML
	public void editCaption(ActionEvent e) {
		Photo current = this.thumbnailListView.getSelectionModel().getSelectedItem();
		current.caption(this.captionText.getText());
		thumbnailListView.refresh();
		this.captionTextField.setText(this.captionText.getText());
		this.captionText.clear();
	}
			
	@FXML StackPane imageStackPane;
	
	/**
	 * Displays the Photo selected to the right side of the window.
	 * Uses a stackPane and sets the background image of the pane to the
	 * photo selected.
	 */
	public void displayPhoto() {
		Photo current = thumbnailListView.getSelectionModel().getSelectedItem();
		if(current==null) {
			//do nothing
			return;
		}
		
		BackgroundImage bgi = new BackgroundImage(current.getImage(), 
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
				BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
		Background bg = new Background(bgi);
		System.out.println(imageStackPane);
		imageStackPane.setBackground(bg);
		
		captionTextField.setText(current.getCaption());
		
		String emptyString = "";
		
		for(int i = 0; i < current.tagList.size(); i++) {
			System.out.println(emptyString);
			emptyString = emptyString.concat(current.tagList.get(i).gettagType()).concat(": ").concat(current.tagList.get(i).gettagInfo()).concat(", ");
		}
		
		if(!emptyString.isEmpty()){
			emptyString = emptyString.substring(0, emptyString.length()-2);
		}
		this.tagTextField.setText(emptyString);
		this.dateTextField.setText(current.getDate().toString());
	}
	
	/**
	 * EventHandler for displaying a photo. Calls displayPhoto()
	 * @param e MouseEvent to determine the photo to display
	 */
	@FXML
	public void displayPhoto(MouseEvent e) {
		displayPhoto();
	}
	
	/**
	 * EventHandler for the button that goes to the next picture. Pressing the
	 * button will select and display the next picture in the list.
	 * @param e ActionEvent for the next button
	 */
	@FXML
	public void nextPicture(ActionEvent e) {
		int i = thumbnailListView.getSelectionModel().getSelectedIndex();
		i = (i+1 >= obsPhotoList.size()) ? 0:i+1;
		thumbnailListView.getSelectionModel().select(i);
		displayPhoto();
	}
	
	/**
	 * EventHandler for the button that goes to the previous picture.
	 * Pressing the button will select and display the previous picture in
	 * the list.
	 * @param e ActionEvent for the previous button
	 */
	@FXML
	public void prevPicture(ActionEvent e) {
		int i = thumbnailListView.getSelectionModel().getSelectedIndex();
		i = (i-1 < 0) ? obsPhotoList.size()-1:i-1;
		thumbnailListView.getSelectionModel().select(i);
		displayPhoto();
	}
	
	/**
	 * Opens the window to add, delete a tag from the photo
	 * @param e ActionEvent for the MenuItem for editing tags
	 */
	@FXML
	public void openTagWindow(ActionEvent e){
		Photo current = this.thumbnailListView.getSelectionModel().getSelectedItem();
		if(current == null){
			Alert nullPhoto = new Alert(Alert.AlertType.ERROR);
			nullPhoto.setContentText("Please select a Photo");
			nullPhoto.show();
		}
		else{
			FXMLLoader tagLoader = new FXMLLoader();
			tagLoader.setLocation(getClass().getResource("/fxml/tagScreen.fxml"));
			
			try{
				tagScreen = tagLoader.load();
				
				Stage stage = new Stage();
				
				TagController tc = tagLoader.getController();
				
				tc.start(current, this);
				
				Scene scene = new Scene(tagScreen);
				stage.setScene(scene);
				stage.setResizable(false);
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.show();
				
				stage.setOnCloseRequest(event -> {
					String emptyString = "";
					
					for(int i = 0; i < current.tagList.size(); i++) {
						System.out.println(emptyString);
						emptyString = emptyString.concat(current.tagList.get(i).gettagType()).concat(": ").concat(current.tagList.get(i).gettagInfo()).concat(", ");
					}
					
					if(!emptyString.isEmpty()){
						emptyString = emptyString.substring(0, emptyString.length()-2);
					}
					this.tagTextField.setText(emptyString);
				});
				
			}catch(IOException | NullPointerException e1){
				e1.printStackTrace();
			}
			
		}
		
	}
	
	/**
	 * EventHandler for the logout menuitem. Calls logout()
	 * from AlbumController and refreshes the album table. 
	 * @param e ActionEvent for the logout menuitem
	 */
	@FXML
	public void logout(ActionEvent e) {
		asc.albumTableView.refresh();
		AlbumController.logout();
		thisStage.close();
	}
}
