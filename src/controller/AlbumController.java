package controller;

/**
 * Controller for Album Screen
 * @author Daniel Kim
 * @author George Ding
 */
import object.*;
import photos.Photos;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlbumController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1403917850212123524L;
	private static Stage loginStage;
	private static Stage thisStage;
	
	/**
	 * User that is currently logged in
	 */
	public static String user;
	
	/**
	 * Observable List for user's albums
	 */
	public static ObservableList<Album> albumList;
	
	/**
	 * ArrayList for user's albums. Loaded in from deserializing and set
	 * for Observable List.
	 */
	public static ArrayList<Album> albumArrList = new ArrayList<Album>();
	
	@FXML AnchorPane renamePopup;
	@FXML AnchorPane findPopup;
	@FXML AnchorPane photoScreen;
	
	@FXML MenuItem createMenuItem;
	@FXML TextField createTextField;
	
	@FXML MenuItem deleteMenuItem;
	@FXML TextField deleteTextField;
	
	@FXML MenuItem renameMenuItem;

	@FXML MenuItem findMenuItem;
	
	@FXML MenuItem logoutMenuItem;
	
	@FXML TableView<Album> albumTableView;
	@FXML TableColumn<Album, String> albumColumn;
	@FXML TableColumn<Album, Integer> itemsColumn;
	@FXML TableColumn<Album, Date> oldestColumn;
	@FXML TableColumn<Album, Date> newestColumn;
	
	/**
	 * Start method for Album Screen. Loads in serialized data for logged
	 * in user.
	 * 
	 * @param loginStage The Login Stage to go back to at logout
	 * @param stage Album Stage
	 * @param user Logged in User
	 */
	@SuppressWarnings("unchecked")
	public void start(Stage loginStage, Stage stage, String user) {
		AlbumController.user = user;
		AlbumController.loginStage = loginStage;
		AlbumController.thisStage = stage;
		
		File stateFile = new File("src/Photo Data/" + user + ".ser");
		
		if(stateFile.exists()) {
			try {
				FileInputStream fis = new FileInputStream(stateFile);
				BufferedInputStream bis = new BufferedInputStream(fis);
				ObjectInputStream ois = new ObjectInputStream(bis);
				
				albumArrList = (ArrayList<Album>) ois.readObject();

				if(albumArrList.size() > 0) {
					for(int i=0; i<albumArrList.size(); i++) {
						
						for(int j=0; j<albumArrList.get(i).getItems(); j++) {
							if(user.equals("stock")) {
								albumArrList.get(i).getPhoto(j).setStock();
							}else {
								albumArrList.get(i).getPhoto(j).setThumbnail();
								albumArrList.get(i).getPhoto(j).setImage();
							}
						}
					}
				}
				
				ois.close();
				bis.close();
				fis.close();
			}catch(IOException | ClassNotFoundException e) {
				System.out.println(e);
			}
		}
		albumList = FXCollections.observableArrayList(albumArrList);
		
		Photos.toSerializeList.add(albumArrList);
		Photos.userList.add(user);
		
		albumTableView.setItems(albumList);
		
		albumColumn.setCellValueFactory(new PropertyValueFactory<Album, String>("name"));
		itemsColumn.setCellValueFactory(new PropertyValueFactory<Album, Integer>("items"));
		oldestColumn.setCellValueFactory(new PropertyValueFactory<Album, Date>("oldestDate"));
		newestColumn.setCellValueFactory(new PropertyValueFactory<Album, Date>("newestDate"));
		
	}
	
	/**
	 * Creates an Album named by the textfield in the menuitem and adds it
	 * to the TableView
	 * 
	 * @param e ActionEvent for create MenuItem
	 */
	@FXML
	public void createAlbum(ActionEvent e) {
		
		
		Album album = new Album(createTextField.getText());
		albumList.add(album);
		albumArrList.add(album);
		createTextField.clear();
	}
	
	/**
	 * deletes album selected in the table at MenuItem click.
	 * @param e ActionEvent for Delete MenuItem
	 */
	@FXML
	public void deleteAlbum(ActionEvent e) {
		
		for(int i=0; i<albumList.size(); i++) {
			if(albumList.get(i).getName().equals(deleteTextField.getText())) {
				albumList.remove(i);
				albumArrList.remove(i);
				deleteTextField.clear();
				return;
			}
		}
	}
	
	/**
	 * Renames the Album Selected in the table. Opens new Window to do this.
	 * @param e ActionEvent for Rename MenuItem
	 */
	@FXML
	public void renameAlbum(ActionEvent e) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/fxml/RenamePopup.fxml"));
		
		try {
			renamePopup = (AnchorPane) loader.load();
			
			RenameAlbumController rac = loader.getController();
			
			Stage stage = new Stage();
			rac.start(stage, this);
			
			Scene scene = new Scene(renamePopup);
			stage.setScene(scene);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setResizable(false);
			stage.show();
		}catch (IOException e1) {
			//shouldnt happen
			System.out.println(e1);
		}
	}
	
	/**
	 * Opens the selected Album when clicked in table. Goes to PhotoController.
	 * 
	 * @param e MouseEvent for opening album from table
	 */
	@FXML
	public void goToAlbum(MouseEvent e) {
	
		Album selected = albumTableView.getSelectionModel().getSelectedItem();
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/fxml/PhotoListScreen.fxml"));
		
		try{
			photoScreen = loader.load();
			
			PhotoController psc = loader.getController();
			
			Stage stage = new Stage();
			stage.setTitle(selected.getName());
			
			psc.normStart(thisStage, stage, selected, this);
			
			thisStage.close();
			
			albumTableView.getSelectionModel().clearSelection();
			Scene scene = new Scene(photoScreen);
			stage.setScene(scene);
			stage.setMaxWidth(855);
			stage.setMaxHeight(430);
			stage.setResizable(false);
			stage.show();
		}catch(IOException | NullPointerException e1) {
			//shouldn't happen
			System.out.println(e1);
		}
	}
	
	/**
	 * Searches for photos by tag or date. Opens a popup to do this.
	 * @param e ActionEvent for search popup menuitem.
	 */
	@FXML
	public void findPhoto(ActionEvent e) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/fxml/FindPopup.fxml"));
		
		try {
			findPopup = loader.load();
			
			SearchController spc = loader.getController();
			
			Stage stage = new Stage();
			
			spc.start(stage, thisStage, photoScreen, this);
			
			Scene scene = new Scene(findPopup);
			stage.setScene(scene);
			stage.setResizable(false);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.showAndWait();
			
			FXMLLoader loader2 = new FXMLLoader();
			loader2.setLocation(getClass().getResource("/fxml/PhotoListScreen.fxml"));
			
			try{
				photoScreen = loader2.load();
				
				PhotoController psc = loader2.getController();
				
				Stage stage2 = new Stage();
				
				psc.searchedStart(thisStage, stage2, this, SearchController.getList());
				
				thisStage.close();
				
				albumTableView.getSelectionModel().clearSelection();
				Scene scene2 = new Scene(photoScreen);
				stage2.setScene(scene2);
				stage2.setMaxWidth(855);
				stage2.setMaxHeight(430);
				stage2.setResizable(false);
				stage2.show();
				
				if(SearchController.getList().isEmpty()){
					Alert empty = new Alert(Alert.AlertType.ERROR);
					empty.setContentText("No results found");
					empty.show();
				}
			}catch(IOException | NullPointerException e1) {
				//shouldn't happen
				System.out.println(e1);
			}
			
		}catch(IOException e1) {
			System.out.println(e1);
		}
	}
	
	/**
	 * Logout on action of menuitem. Calls the static method logout() and refreshes
	 * the table on action.
	 * @param e ActionEvent for logout menuitem
	 */
	@FXML
	public void logout(ActionEvent e) {
		
		this.albumTableView.refresh();
		logout();
	}
	
	/**
	 * Static logout call used by this controller and photocontroller.
	 * Serializes Album list on action and goes back to Login Screen.
	 */
	public static void logout() {
		
		try {
			FileOutputStream fos = new FileOutputStream("src/Photo Data/" + user + ".ser");
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			
			oos.writeObject(albumArrList);
			
			Photos.toSerializeList.remove(albumArrList);
			Photos.userList.remove(user);
			
			oos.close();
			bos.close();
			fos.close();
		} catch (FileNotFoundException e1) {
			
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		loginStage.show();
		AlbumController.thisStage.close();
	}
	
}
